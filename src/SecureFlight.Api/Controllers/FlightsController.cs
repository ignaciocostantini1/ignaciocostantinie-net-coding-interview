﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IService<Flight> flightService, IMapper mapper, IRepository<Passenger> passengerRepository, IRepository<Flight> flightRepository)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPut]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassanger([FromQuery] string passangerId, FlightDataTransferObject flightDto){
        var selecterdPassanger = await passengerRepository.GetByIdAsync(passangerId);
        var flight = await flightRepository.GetByIdAsync(flightDto.Id);

        if(selecterdPassanger == null){
            return NotFound($"Passenger with id {passangerId} was not found.");
        }
        if(flight == null){
            return NotFound($"Flight with id {flightDto.Id} was not found.");
        }

        flight.Passengers.Add(selecterdPassanger);
        var result = flightRepository.Update(flight);
        
        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(result);
    }

    [HttpPut]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> RemovePassanger([FromQuery] string passengerId, FlightDataTransferObject flightDto){
        var selecterdPassanger = await passengerRepository.GetByIdAsync(passengerId);
        var flight = await flightRepository.GetByIdAsync(flightDto.Id);

        if(selecterdPassanger == null){
            return NotFound($"Passenger with id {passengerId} was not found.");
        }
        if(flight == null){
            return NotFound($"Flight with id {flightDto.Id} was not found.");
        }

        if(flight.Passengers.Any(passenger => passenger.Id == passengerId)){
            flight.Passengers.Remove(selecterdPassanger);
        }

        // if(selecterdPassanger.Flights.Count == 0){
        //     selecterdPassanger.
        //     passengerRepository.Update(passenger);
        // }   


        var result = flightRepository.Update(flight);
        
        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(result);
    }
}